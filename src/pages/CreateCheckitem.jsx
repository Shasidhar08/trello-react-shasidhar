import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import { useState, useRef } from "react";

export default function CreateCheckitem({
  checklistId,
  checkitemsName,
  createCheckitemHandler,
}) {
  const [isAddCheckitem, setIsAddCheckitem] = useState(false);
  const inputRef = useRef();
  return (
    <div
      style={{
        marginTop: ".5rem",
        display: "flex",
        flexDirection: "column",
        width: "12rem",
      }}>
      {isAddCheckitem && (
        <input ref={inputRef} placeholder="Add Item" focused="true" />
      )}
      <Button
        onClick={() => {
          setIsAddCheckitem((item) => !item);
          isAddCheckitem && createCheckitemHandler(inputRef.current.value);
        }}>
        Add Item
      </Button>
    </div>
  );
}
