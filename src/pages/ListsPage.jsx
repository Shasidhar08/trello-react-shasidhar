import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import GetCardsInList from "./GetCardsInList";
import { ListsAPI, CardsAPI } from "../apis/APIStore";
import {
  Card,
  CircularProgress,
  Container,
  Button,
  TextField,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import AddIcon from "@mui/icons-material/Add";

export default function ListsPage() {
  const { id } = useParams();
  const [listsData, setListsData] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);
  const [isAddList, setIsAddList] = useState(false);
  const [listName, setListName] = useState("");
  useEffect(() => {
    ListsAPI.getLists(id)
      .then((lists) => {
        setListsData(lists.data);
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
        setError(err);
      });
  }, [id]);

  const createListHandler = () => {
    if (listName.trim() !== "") {
      ListsAPI.createList(id, listName)
        .then((response) => {
          setListsData([...listsData, response.data]);
        })
        .catch((err) => {
          setError(err);
        });
    }
  };

  const deleteListHandler = (listId) => {
    ListsAPI.deleteList(listId)
      .then((response) => {
        let data = listsData.filter((lst) => lst.id !== listId);
        setListsData(data);
      })
      .catch((err) => {
        setError(err.message);
      });
  };

  if (error) {
    return (
      <h1
        style={{
          textAlign: "center",
        }}>
        {error.message}
      </h1>
    );
  }
  if (loading) return <CircularProgress />;
  else
    return (
      <div className="lists">
        <div
          style={{
            display: "flex",
            columnGap: "1rem",
            overflowX: "scroll",
          }}>
          {listsData.length > 0 &&
            Array.isArray(listsData) &&
            listsData.map((list, index) => {
              return (
                <Card
                  className="list"
                  key={list.id}
                  style={{
                    overflowY: "auto",
                    scrollbarGutter: "stable",
                  }}>
                  <div
                    style={{
                      marginBottom: "1rem",
                      display: "flex",
                      justifyContent: "space-between",
                    }}>
                    <h2>{list.name}</h2>
                    <DeleteIcon
                      onClick={(event) => {
                        deleteListHandler(list.id);
                      }}
                      style={{
                        cursor: "pointer",
                      }}></DeleteIcon>
                  </div>
                  <GetCardsInList listId={list.id} />
                </Card>
              );
            })}
          <Card className="list">
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                cursor: "pointer",
                backgroundColor: "#dff7b9",
              }}
              onClick={() => {
                setIsAddList((isAddList) => !isAddList);
              }}>
              <AddIcon />
              Create List
            </div>
            {isAddList && (
              <>
                <TextField
                  onChange={(event) => {
                    setListName(event.target.value);
                  }}
                />
                <Button
                  variant="contained"
                  onClick={() => {
                    createListHandler();
                  }}>
                  Add List
                </Button>
              </>
            )}
          </Card>
        </div>
      </div>
    );
}
