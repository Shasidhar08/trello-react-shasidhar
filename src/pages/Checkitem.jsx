import { useState, useEffect } from "react";
import DeleteIcon from "@mui/icons-material/Delete";

export default function Checkitem({
  checkitem,
  updateCheckitemHandler,
  deleteCheckitemHandler,
}) {
  const [isChecked, setIsChecked] = useState(false);
  useEffect(() => {
    setIsChecked(checkitem.state === "complete" ? true : false);
  }, []);
  return (
    <>
      <input
        type="checkbox"
        name="checkitem"
        id={checkitem.id}
        checked={isChecked}
        onChange={(e) => {
          setIsChecked(!isChecked);
          updateCheckitemHandler(
            checkitem.id,
            e.target.checked ? "complete" : "incomplete"
          );
        }}
      />
      <label htmlFor={checkitem.id}>{checkitem.name}</label>
      <DeleteIcon
        onClick={() => {
          deleteCheckitemHandler(checkitem.id);
        }}
        style={{ cursor: "pointer" }}></DeleteIcon>
    </>
  );
}
