import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { BoardsAPI, ListsAPI } from "../apis/APIStore";
import LoadingComponent from "../components/Loading";
import { DisplayBoard, CreateBoardComponent } from "../components/Board";

export default function BoardsPage() {
  const [boardData, setBoardData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [boardName, setBoardName] = useState("");

  useEffect(() => {
    BoardsAPI.getBoards()
      .then((boards) => {
        setBoardData(boards.data);
        setLoading(false);
      })
      .catch((err) => {
        setError(err);
        setLoading(false);
      });
  }, []);

  let createBoardHandler = () => {
    BoardsAPI.createBoard(boardName)
      .then((board) => {
        setBoardData([...boardData, board.data]);
      })
      .catch((err) => {
        setError(err);
      });
  };

  if (error) {
    return <h1>Some Error Occured</h1>;
  }

  return (
    <>
      {loading ? (
        <LoadingComponent />
      ) : (
        <>
          {boardData.length > 0 &&
            Array.isArray(boardData) &&
            boardData.map((board, index) => {
              return (
                <Link key={board.id} to={`/boards/${board.id}`}>
                  <DisplayBoard index={index} board={board} />
                </Link>
              );
            })}
          <CreateBoardComponent
            setBoardName={setBoardName}
            setBoardData={setBoardData}
            createBoardHandler={createBoardHandler}
          />
        </>
      )}
    </>
  );
}
