import { useState, useEffect } from "react";
import { CardsAPI } from "../apis/APIStore.js";
import { CardComponent, CreateCardComponent } from "../components/Card";
import CircularProgress from "@mui/material/CircularProgress";

export default function GetCardsInList({ listId }) {
  const [cards, setCards] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const [cardName, setCardName] = useState("");

  useEffect(() => {
    CardsAPI.getCards(listId)
      .then((cardData) => {
        setLoading(false);
        setCards(cardData.data);
      })
      .catch((error) => {
        setLoading(false);
        setError(error.message);
      });
  }, []);

  let createCardHandler = () => {
    CardsAPI.createCard(listId, cardName)
      .then((card) => {
        setCards([...cards, card.data]);
      })
      .catch((error) => {
        setError(error.message);
      });
  };

  let deleteCardHandler = (cardId) => {
    CardsAPI.deleteCard(cardId)
      .then((card) => {
        let allCards = cards.filter((c) => c.id !== cardId);
        setCards([...allCards]);
      })
      .catch((error) => {
        console.error(error);
        setLoading(false);
        setError(error.message);
      });
  };

  if (error) {
    return <h1>{error}</h1>;
  }
  if (loading) {
    return <CircularProgress />;
  } else {
    return (
      <div>
        {cards.length > 0 &&
          Array.isArray(cards) &&
          cards.map((card) => {
            return (
              <CardComponent
                key={card.id}
                deleteCardHandler={deleteCardHandler}
                card={card}
              />
            );
          })}
        <CreateCardComponent
          createCardHandler={createCardHandler}
          setCardName={setCardName}
        />
      </div>
    );
  }
}
