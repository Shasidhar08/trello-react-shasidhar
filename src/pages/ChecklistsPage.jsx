import { checkitemsAPI } from "../apis/APIStore.js";
import { useState, useEffect } from "react";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import PlaylistAddCheckIcon from "@mui/icons-material/PlaylistAddCheck";
import DeleteIcon from "@mui/icons-material/Delete";
import Checkitem from "./Checkitem";
import CreateCheckitem from "./CreateCheckitem";

export default function Checklist({
  cardId,
  checklist,
  deleteChecklistHandler,
}) {
  const [checkitems, setCheckitems] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    checkitemsAPI
      .getCheckitems(checklist.id)
      .then((response) => {
        setCheckitems(response.data);
      })
      .catch((error) => {
        setError(error.message);
      });
  }, []);

  const createCheckitemHandler = (checkitemName) => {
    checkitemsAPI
      .createCheckitem(checklist.id, checkitemName)
      .then((response) => {
        setCheckitems([...checkitems, response.data]);
      })
      .catch((error) => {
        setError(error.message);
      });
  };
  const deleteCheckitemHandler = (checkitemId) => {
    checkitemsAPI
      .deleteCheckitem(checklist.id, checkitemId)
      .then((response) => {
        let filtered = checkitems.filter(
          (checkitem) => checkitem.id !== checkitemId
        );
        setCheckitems(filtered);
      })
      .catch((error) => {
        setError(error.message);
      });
  };

  const updateCheckitemHandler = (checkItemId, isState) => {
    console.log(cardId + " " + checkItemId + " " + isState);
    checkitemsAPI
      .updateCheckitem(cardId, checkItemId, isState)
      .catch((error) => {
        setError(error.message);
      });
  };

  if (error) {
    return (
      <h1
        style={{
          textAlign: "center",
        }}>
        {error.message}
      </h1>
    );
  }

  return (
    <div
      style={{
        boxShadow:
          "rgba(9, 30, 66, 0.25) 0px 4px 8px -2px, rgba(9, 30, 66, 0.08) 0px 0px 0px 1px",
      }}>
      <div style={{ marginBottom: "1rem", display: "flex", gap: "1rem" }}>
        <PlaylistAddCheckIcon />
        <h3>{checklist.name}</h3>
        <DeleteIcon
          style={{ cursor: "pointer" }}
          onClick={(event) => {
            event.stopPropagation();
            deleteChecklistHandler(checklist.id);
          }}
        />
      </div>
      <div style={{ margin: "1rem 1rem 2rem" }}>
        {checkitems.length > 0 &&
          Array.isArray(checkitems) &&
          checkitems.map((checkitem) => {
            return (
              <div key={checkitem.id} style={{ display: "flex", gap: "1rem" }}>
                <Checkitem
                  checkitem={checkitem}
                  deleteCheckitemHandler={deleteCheckitemHandler}
                  updateCheckitemHandler={updateCheckitemHandler}
                />
              </div>
            );
          })}
        <CreateCheckitem createCheckitemHandler={createCheckitemHandler} />
      </div>
    </div>
  );
}

export function CreateChecklist({ createChecklistHandler, setChecklistName }) {
  const [isAddChecklist, setIsAddChecklist] = useState(false);

  return (
    <div>
      {!isAddChecklist && (
        <Button
          variant="contained"
          onClick={() => {
            setIsAddChecklist((prevValue) => !prevValue);
          }}>
          Create Checklist
        </Button>
      )}
      {isAddChecklist && (
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            width: "15rem",
            gap: "1rem",
          }}>
          <TextField
            placeholder="Add checklist"
            onChange={(event) => {
              setChecklistName(event.target.value);
            }}
          />
          <Button
            variant="contained"
            onClick={() => {
              createChecklistHandler();
              setIsAddChecklist((prevValue) => !prevValue);
            }}>
            Add
          </Button>
        </div>
      )}
    </div>
  );
}
