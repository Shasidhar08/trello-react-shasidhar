import { API_KEY, API_TOKEN } from "../Auth";
import axios from "axios";

const BoardsAPI = {
  getBoards: function () {
    return axios.get(
      `https://api.trello.com/1/members/me/boards?key=${API_KEY}&token=${API_TOKEN}`
    );
  },
  createBoard: function (name) {
    return axios.post(
      `https://api.trello.com/1/boards/?name=${name}&key=${API_KEY}&token=${API_TOKEN}`
    );
  },
  deleteBoard: function (boardId) {
    return axios.delete(
      `https://api.trello.com/1/boards/${boardId}?key=${API_KEY}&token=${API_TOKEN}`
    );
  },
};
const ListsAPI = {
  getLists: function (boardId) {
    return axios.get(
      `https://api.trello.com/1/boards/${boardId}/lists?key=${API_KEY}&token=${API_TOKEN}`
    );
  },
  createList: function (boardId, name) {
    return axios.post(
      `https://api.trello.com/1/boards/${boardId}/lists?name=${name}&key=${API_KEY}&token=${API_TOKEN}`
    );
  },
  deleteList: function (listId) {
    return axios.put(
      `https://api.trello.com/1/lists/${listId}/closed?value=true&key=${API_KEY}&token=${API_TOKEN}`
    );
  },
};
const CardsAPI = {
  getCards: function (listId) {
    return axios.get(
      `https://api.trello.com/1/lists/${listId}/cards?key=${API_KEY}&token=${API_TOKEN}`
    );
  },
  createCard: function (listId, cardName) {
    return axios.post(`https://api.trello.com/1/cards`, {
      name: cardName,
      idList: listId,
      key: API_KEY,
      token: API_TOKEN,
    });
  },
  deleteCard: function (cardId) {
    return axios.delete(
      `https://api.trello.com/1/cards/${cardId}?key=${API_KEY}&token=${API_TOKEN}`
    );
  },
};
const checklistsAPI = {
  getChecklists: function (cardId) {
    return axios.get(
      `https://api.trello.com/1/cards/${cardId}/checklists?key=${API_KEY}&token=${API_TOKEN}`
    );
  },
  createChecklist: function (cardId, checklistName) {
    return axios.post(
      `https://api.trello.com/1/checklists?idCard=${cardId}&key=${API_KEY}&token=${API_TOKEN}`,
      { name: checklistName }
    );
  },
  deleteChecklist: function (cardID, checklistId) {
    return axios.delete(
      `https://api.trello.com/1/checklists/${checklistId}?key=${API_KEY}&token=${API_TOKEN}`
    );
  },
};

const checkitemsAPI = {
  createCheckitem: function (checklistId, checkitemName) {
    return axios.post(
      `https://api.trello.com/1/checklists/${checklistId}/checkItems?name=${checkitemName}&key=${API_KEY}&token=${API_TOKEN}`
    );
  },
  getCheckitems: function (checkItemsId) {
    return axios.get(
      `https://api.trello.com/1/checklists/${checkItemsId}/checkItems?key=${API_KEY}&token=${API_TOKEN}`
    );
  },
  updateCheckitem: function (cardId, checkItemId, isState) {
    return axios.put(
      `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?state=${isState}&key=${API_KEY}&token=${API_TOKEN}`
    );
  },

  deleteCheckitem: function (checklistId, checkitemId) {
    return axios.delete(
      `https://api.trello.com/1/checklists/${checklistId}/checkItems/${checkitemId}?key=${API_KEY}&token=${API_TOKEN}`
    );
  },
};
export { ListsAPI, BoardsAPI, CardsAPI, checklistsAPI, checkitemsAPI };
