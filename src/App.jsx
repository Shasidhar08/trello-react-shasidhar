import "./App.css";
import { Route, Routes } from "react-router-dom";
import BoardsPage from "./pages/BoardsPage";
import ListsPage from "./pages/ListsPage";
import ButtonAppBar from "./components/ButtonAppBar";

function App() {
  return (
    <>
      <ButtonAppBar />
      <Routes>
        <Route path="/" element={<BoardsPage />} />
        <Route path="/boards/:id" element={<ListsPage />} />
      </Routes>
    </>
  );
}

export default App;
