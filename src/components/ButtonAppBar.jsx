import React from "react";
import { Link } from "react-router-dom";
import { styled } from "@mui/system";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import HomeIcon from "@mui/icons-material/Home";

const StyledAppBar = styled(AppBar)(({ theme }) => ({
  backgroundColor: "#67f1d6",
}));

const Navbar = () => {
  return (
    <StyledAppBar position="static">
      <Link to="/">
        <img
          style={{
            height: "1.5rem",
            marginLeft: "1rem",
            padding: "1rem",
          }}
          src="https://www.vectorlogo.zone/logos/trello/trello-official.svg"
          alt="Trello Logo and Home button"
        />
      </Link>
    </StyledAppBar>
  );
};

export default Navbar;
