import * as React from "react";
import { useState, useEffect } from "react";
import { checklistsAPI } from "../apis/APIStore.js";
import Checklist, { CreateChecklist } from "../pages/ChecklistsPage";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import { CircularProgress } from "@mui/material";
import FormatListBulletedIcon from "@mui/icons-material/FormatListBulleted";

const Style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  minWidth: "40rem",
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

export default function BasicModal({ cardName, cardId, open, handleClose }) {
  const [checklistsData, setChecklistsData] = useState([]);
  const [error, setError] = useState(null);
  const [checklistName, setChecklistName] = useState("");

  useEffect(() => {
    checklistsAPI
      .getChecklists(cardId)
      .then((checklists) => {
        setChecklistsData(checklists.data);
      })
      .catch((err) => {
        setError(err);
      });
  }, []);

  if (error) {
    return (
      <h1
        style={{
          textAlign: "center",
        }}>
        {error.message}
      </h1>
    );
  }

  const createChecklistHandler = () => {
    checklistsAPI
      .createChecklist(cardId, checklistName)
      .then((response) => {
        console.log(response.data);
        setChecklistsData([...checklistsData, response.data]);
      })
      .catch((error) => {
        setError(error.message);
      });
  };

  const deleteChecklistHandler = (checklistId) => {
    checklistsAPI
      .deleteChecklist(cardId, checklistId)
      .then(() => {
        let allChecklist = checklistsData.filter(
          (checklist) => checklist.id !== checklistId
        );
        setChecklistsData([...allChecklist]);
      })
      .catch((error) => {
        console.error(error);
        setError(error.message);
      });
  };

  return (
    <Modal
      open={open}
      onClose={(event) => {
        handleClose(event);
      }}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description">
      <Box sx={Style}>
        <div style={{ display: "flex", alignItems: "center", gap: "1rem" }}>
          <FormatListBulletedIcon />
          <h2>{cardName}</h2>
        </div>
        <div style={{ marginTop: "1rem" }}>
          {checklistsData &&
            checklistsData.map((checklist) => {
              return (
                <Checklist
                  key={checklist.id}
                  cardId={cardId}
                  checklist={checklist}
                  deleteChecklistHandler={deleteChecklistHandler}
                />
              );
            })}

          <CreateChecklist
            createChecklistHandler={createChecklistHandler}
            setChecklistName={setChecklistName}
          />
        </div>
      </Box>
    </Modal>
  );
}
