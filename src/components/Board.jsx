import { Add } from "@mui/icons-material";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import { useState } from "react";

const boardStyle = {
  color: "white",
  width: "15rem",
  height: "8rem",
  padding: "1rem",
  margin: "20px",
  float: "left",
  borderRadius: "10px",
  boxShadow: "3px 3px .5em #f4c085, -.5em 0 .4em #f1d667",
};

export function DisplayBoard({ index, board }) {
  return (
    <>
      <div
        className="dashboardCard"
        style={{
          ...boardStyle,
          backgroundImage: board.prefs.backgroundImage
            ? `url(${board.prefs.backgroundImage})`
            : `url("https://picsum.photos/300/200?blur=1&random=${index}")`,
          cursor: "pointer",
          backgroundSize: "cover",
          backgroundPosition: "center",
        }}>
        <div
          className="dashboardContent"
          style={{
            color: "white",
            fontWeight: "bold",
            fontSize: "1.5em",
            textShadow: "0px 1px 10px black",
          }}>
          {board.name}
        </div>
      </div>
    </>
  );
}
export function CreateBoardComponent({
  setBoardData,
  setBoardName,
  createBoardHandler,
}) {
  const [add, setAdd] = useState(false);
  // console.log(add);
  let createBoardStyle = {
    display: "flex",
    flexWrap: "wrap",
    rowGap: "1rem",
  };
  return (
    <div
      style={{
        ...boardStyle,
        backgroundImage: `url("https://picsum.photos/300/200?blur=2")`,
      }}>
      <div
        onClick={() => {
          setAdd((add) => !add);
        }}
        style={{
          cursor: "pointer",
          fontWeight: "bold",
          fontSize: "1.5rem",
          textShadow: "0px 1px 10px black",
        }}>
        {add ? "- Board" : "+ Board"}
      </div>
      {add && (
        <div style={createBoardStyle}>
          <TextField
            color="secondary"
            InputProps={{
              inputProps: { style: { color: "white" } },
            }}
            InputLabelProps={{
              style: { color: "white", textShadow: "2px 4px 5px black" },
            }}
            id="outlined-basic"
            label="Board Name"
            variant="standard"
            onChange={(event) => addBoardName(event.target.value)}
          />
          <Button
            onClick={(event) => {
              console.log("click");
              setAdd((add) => !add);
              createBoardHandler();
            }}
            variant="contained">
            Create Board
          </Button>
        </div>
      )}
    </div>
  );
}
