import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import BasicModal from "./Modal";
import { useState } from "react";
import AddIcon from "@mui/icons-material/Add";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { CardsAPI } from "../apis/APIStore.js";

export function CardComponent({ deleteCardHandler, card }) {
  const [open, setOpen] = useState(false);
  const handleOpen = (event) => {
    event.stopPropagation();
    setOpen(true);
  };
  const handleClose = (event) => {
    event.stopPropagation();
    setOpen(false);
  };
  let Style = {
    color: "black",
    borderRadius: "5px",
    boxShadow: `rgba(0, 0, 0, 0.24) 0px 3px 8px`,
    padding: "1rem",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    margin: "1rem 0rem",
    cursor: "pointer",
  };
  return (
    <div key={card.id}>
      <div
        className="Card"
        style={Style}
        onClick={(event) => handleOpen(event)}>
        <span>{card.name}</span>
        <DeleteForeverIcon
          onClick={(event) => {
            event.stopPropagation();
            deleteCardHandler(card.id);
          }}
          style={{ cursor: "pointer", color: "#D63939" }}></DeleteForeverIcon>
      </div>

      <BasicModal
        cardId={card.id}
        open={open}
        cardName={card.name}
        handleClose={handleClose}
      />
    </div>
  );
}

export function CreateCardComponent({ createCardHandler, setCardName }) {
  const [isAddCard, setIsAddCard] = useState(false);
  const [error, setError] = useState(false);

  return (
    <>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          cursor: "pointer",
          boxShadow: `rgba(0, 0, 0, 0.24) 0px 3px 8px`,
          backgroundColor: "#abdbe3",
          marginBottom: "1rem",
        }}
        onClick={() => {
          setIsAddCard((isAddCard) => !isAddCard);
        }}>
        <AddIcon />
        Create Card
      </div>
      {isAddCard && (
        <div>
          <TextField
            id="outlined-basic"
            label="Card Name"
            variant="outlined"
            onChange={(event) => setCardName(event.target.value)}
          />
          <Button
            onClick={(event) => {
              setIsAddCard((isAddCard) => !isAddCard);
              createCardHandler();
            }}
            variant="contained">
            Add Card
          </Button>
        </div>
      )}
    </>
  );
}
